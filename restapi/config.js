const { env } = require("process");

const environments = {
  staging: {
    	httpPort: 3000,
	httpsPort: 3001,
    	envName: 'staging'
  },
  production: {
    httpPort: 5000,
	  httpsPort: 5001,
    envNamne: 'production'
  }
}

let chosenEnv = typeof(process.env.NODE_ENV) == 'string' ? 
  process.env.NODE_ENV : 
  ''
let currentEnv = typeof(environments[chosenEnv]) == 'object' ? 
  environments[chosenEnv] : 
  environments.staging

module.exports = currentEnv
