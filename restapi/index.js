/*
 *
 * */

// Dependencies

const HTTP = require('http')
const HTTPS = require('https')
const URL = require('url')
const StringDecoder = require('string_decoder').StringDecoder
const CONFIG = require('./config')
const FS = require('fs')

// response ever with a string
const httpServer = HTTP.createServer( function(req, res) {
	genericServer(req, res)
})
// start the server at port 3000
httpServer.listen(CONFIG.httpPort, function(){
	console.log(`http port: ${CONFIG.httpPort}`)
})
let httpsServerOptions = {
	key: FS.readFileSync('./https/key.pem'),
	cert: FS.readFileSync('./https/cert.pem')
}
const httpsServer = HTTPS.createServer(httpsServerOptions, function(req, res){
	genericServer(req, res)
})
httpsServer.listen(CONFIG.httpsPort, function(){
	console.log(`https port: ${CONFIG.httpsPort}`)
})
let handlers = {}
handlers.notFound = function(data, callback){
	callback(404)	
}
handlers.ping = function(data, callback){
	callback(200)
}
const router = {
	'ping': handlers.ping
}

let genericServer = function(req, res) {
        let parsedUrl = URL.parse(req.url, true)
        let path = parsedUrl.pathname
        let trimmedPath = path.replace(/^\/+|\/+$/g,'')
        let queryParams = parsedUrl.query
        let method = req.method
        let headers = req.headers
        let decoder = new StringDecoder('utf-8')
        let buffer = ''
        req.on('data', function(data){
                buffer += decoder.write(data)
        })
        req.on('end', function(){
                buffer += decoder.end()
                let chosenHandler = typeof(router[trimmedPath]) !== 'undefined'? 
                        router[trimmedPath] : 
                        handlers.notFound
                let data = {
                        path: trimmedPath,
                        queryString: queryParams,
                        method: method,
                        headers: headers,
                        payload: buffer
                }
                chosenHandler(data, function(statusCode, payload) {
                        statusCode = typeof(statusCode) == 'number'? 
                                statusCode : 
                                200
                        payload = typeof(payload) == 'object'?
                                payload :
                                {}
                        res.setHeader('Content-Type','application/json')
                        res.writeHead(statusCode)
                        res.end(JSON.stringify(payload))
                        console.log('Handling request', statusCode, payload)      
                }) 
        })	
}
